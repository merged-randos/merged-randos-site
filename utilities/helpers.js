import { useEffect } from 'react';

// add and remove event listener on each render
export const useEvent = (event, handler, passive = false) => {
	useEffect(() => {
		window.addEventListener(event, handler, passive);
		return function cleanup() {
			window.removeEventListener(event, handler);
		};
	});
};

export const convertTimestamp = (timestamp) => {
	return new Date(timestamp).toLocaleDateString([], { hour: '2-digit', minute: '2-digit' });
};

// swr fetch function
export const fetcher = (...args) => fetch(...args).then((res) => res.json());
