import Link from 'next/link';
import Image from 'next/image';

export default function Header() {
	return (
		<header>
			<Link href={'/'}>
				<a className="logo">
					<Image src={'/logo.png'} width={40} height={40} />
				</a>
			</Link>
			<nav>
				<Link href="/">Home</Link>
				{` / `}
				<Link href="/about">About</Link>
				{` / `}
				<a href="https://twitter.com/mergedrandos" target="_blank">
					Twitter
				</a>
				{` / `}
				<a href="https://apps.apple.com/us/app/merge-randos/id1550518247" target="_blank">
					App
				</a>
			</nav>
			<style jsx>{`
				header {
					margin-bottom: 10px;
					display: flex;
					align-items: flex-end;
				}
				a {
					padding-bottom: 3px;
					line-height: 0;
				}
				nav {
					line-height: 1em;
					font-weight: bold;
				}
				.logo {
					padding-right: 7px;
				}
			`}</style>
		</header>
	);
}
