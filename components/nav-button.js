import { Fragment } from 'react';

export default function NavButton({ data, direction, handleNav }) {
	const text = direction === 'previous' ? '<<prev' : 'next>>';
	const buttonClass =
		(direction === 'previous' && data?.next && !data?.previous) || (direction === 'next' && data?.previous && !data?.next)
			? 'button-inactive'
			: null;
	return (
		<Fragment>
			<button className={buttonClass} onClick={() => handleNav(direction)}>
				{text}
			</button>
			<style jsx>{`
				button {
					border: 0;
					background-color: transparent;
					font-weight: bold;
					cursor: pointer;
					color: var(--color-1);
				}
				.button-inactive {
					color: var(--color-gray);
					cursor: default;
				}
			`}</style>
		</Fragment>
	);
}
