import Head from 'next/head';
import { Fragment } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { useState } from 'react';
import useSWR from 'swr';

import { fetcher } from '../utilities/helpers';
const i = 48;

export default function Index(props) {
	const [postCount, setPostCount] = useState(i);
	const [page, setPage] = useState(0);
	const { data, error } = useSWR(`${process.env.NEXT_PUBLIC_URI}/posts?page=${page}&i=${i}&home=${true}`, fetcher, {
		initialData: props.posts,
		revalidateOnFocus: false,
	});
	const [postArr, setpostArr] = useState(data);
	const buttonDisplay = postArr.length > postCount ? 'display-block' : 'display-none';
	const handleLoadMoreClick = () => {
		postCount + i < postArr.length ? setPostCount(postCount + i) : setPostCount(postArr.length);
		setPage(page + 1);
	};
	if (data[0] && !postArr.includes(data[0])) {
		setpostArr([...postArr, ...data]);
	}

	const posts = postArr.slice(0, postCount).map((post, index) => (
		<div key={index}>
			<Link href={`/${post.slug}`}>
				<a>
					<Image src={`${process.env.NEXT_PUBLIC_URI}/images/${post.image}`} alt={post.title} width={275} height={275} />
				</a>
			</Link>
		</div>
	));
	if (error) {
		return <Fragment>error</Fragment>;
	}
	return (
		<Fragment>
			<Head>
				<title>Merged Randos</title>
			</Head>
			<main>
				<div className="grid-container">{posts}</div>
				<div className="flex-container">
					<button className={buttonDisplay} onClick={() => handleLoadMoreClick()} type="button">
						More
					</button>
				</div>
			</main>
			<style jsx>{`
				.grid-container {
					display: grid;
					grid-template-columns: repeat(auto-fill, 275px);
					row-gap: 2.8em;
					column-gap: 3em;
					justify-items: stretch;
				}
				.flex-container {
					display: flex;
					justify-content: center;
					margin: 50px 0;
				}
				button {
					padding: 10px 30px;
					font-weight: bold;
					font-size: 1.2em;
					color: #d800ea;
					border-color: #d800ea;
					background-color: transparent;
					cursor: pointer;
				}
				.display-block {
					display: block;
				}
				.display-none {
					display: none;
				}
			`}</style>
		</Fragment>
	);
}

export async function getServerSideProps() {
	const posts = await fetcher(`${process.env.NEXT_PUBLIC_URI}/posts?page=0&i=${i}&home=${true}`);
	return { props: { posts } };
}
