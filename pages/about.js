import Head from 'next/head';
import Image from 'next/image';
import { Fragment } from 'react';
import ReactMarkdownWithHtml from 'react-markdown/with-html';

export default function About() {
	return (
		<Fragment>
			<Head>
				<title>About Merging the Randos</title>
			</Head>
			<article>
				<Image src={'/about-header.png'} width={960} height={407} />
				<ReactMarkdownWithHtml children={md} allowDangerousHtml />
			</article>
		</Fragment>
	);
}

const md = `
# About About
Published 2/2/21

What this site does is pick two terms randomly from this [list of 3000 common words](https://gitlab.com/merged-randos/merged-randos-server/-/blob/master/my_modules/terms.js), uses them to search for images, combines and crops the images, and then posts them here. You can merge your own randos with [the iPhone app](https://apps.apple.com/us/app/merge-randos/id1550518247).

This piece will be mostly technical but before all that I'd like to say regarding art that random double exposures are cool because they mess with our conceptual mind. You can feel yourself trying to make sense of them—when of course they make no sense. Also they're funny. And occasionally you get a good composition. Even the bad compositions are interesting in their own way. If there's anyone who still uses film, just shoot a roll, then wind it back and shoot again: the original merged randos. As for the design of this site: defaults baby, craigstlist vibes. 
	    	    	
I was like "what is [Next.js](https://nextjs.org)?" and a couple weeks later had built a Next site, a [Node](https://nodejs.org) app/server, and a [React Native](https://reactnative.dev) iPhone app. Ha. You can find [the code for all those on Gitlab](https://gitlab.com/merged-randos). Just FYI anyone looking for a tutorial this is not it, sorry. Though you may acquire some more general understanding, who knows.   	
	    	
I was interested in Next because I like a static site generators. Hitting a database when you don't need to is bad in multiple ways, its: slow, costly, and insecure. Also I feel like there's just something more authentically internety about static sites than the current practice of making a site that's actually an app. I've used [Jekyll](https://jekyllrb.com/) and [Middleman](https://middlemanapp.com) in the past and had build a project with [Gatsby](https://www.gatsbyjs.com) so I felt like Next was *next* up (haha).
	    	
Next, it should be noted, is not exactly a static site generator. It's an isomorphic single page app generator, built on top of [React](https://reactjs.org), that does static optimizations. That is to say it builds sites that have both server and client side rendering and makes them static where it can. Though you can use Next for purely static generation if you want.

This site is kind of a silly project for Next. The Next.js framework is intended for big production apps that need to be as fast as possible and get the most out of their resources. Where this is a tiny three page site that no one will ever visit. Also there's a number of cloud deployment schemes intended to make next hum but I just doinked it on a linux server. I did get some familiarity with the framework out of this project, though obviously I never reached the pain points of trying to get it to do what it's really designed for: making big sites run good.
	    	
I set out to build a Next app—but when I decided to do double exposures I needed some way of creating the images—and that's so not gonna happen on the front end. Enter our old friend Node. I feel like this is turning into one of those recipe sites where there's 2000 words of inane blogging before you get to the recipe, except in this case there's also no recipe.
	    	
## The Node App
	    	
The Node app searches a couple sites for [Creative Commons licensed](https://creativecommons.org/licenses) images, combines them into a double exposure, tweets the image out, and serves it and its data up for the next app to consume. I used [Request](https://github.com/request/request) for scraping, [Cheerio](https://cheerio.js.org) for parsing the scraped pages, [GraphicsMagick](https://aheckmann.github.io/gm/) to process the images, [Twit](https://github.com/ttezel/twit) to tweet, [Apollo](https://www.apollographql.com) to expose a graphql endpoint, and [Mongo](https://www.mongodb.com) for the data store. 

I structured [the image acquisition and processing part of the app](https://gitlab.com/merged-randos/merged-randos-server/-/tree/master/my_modules) as a promise chain using <code>then()</code>, which is such a nice convention when you have functions that need to pass data down the line. Remember callback hell, wow. And it's straight javascript, no library. Just look at that, beautiful.
	    	
~~~
	chooseTerm()
	.then((result) => fetchData(result))
	.then((result) => downloadImages(result))
	.then((result) => calculateImageCrop(result))
	...
~~~
	    	
One funny thing was both sites I'm pulling images from, [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page) and [Creative Commons](https://creativecommons.org), have apis—but I looked at them, saw the word token, and my eyes glazed over. I was all "let just scrape it". So I scraped it. And the very next day the app stopped working. Creative Commons had unveiled a new design overnight. I did fix it with only a little bit of trouble. Though Creative Commons did something kind of annoying and substandard, which I detailed in the <code>parsePage()</code> comments if you care about such things. The code altogether is somewhat commented up if you want to take a look at how the sausage gets made. Feel free to get in touch and tell me where I effed up. Email is at the bottom of this page. There may be be some error handling weirdness where promises are nested, and tests would be good. I could write some tests. 

**Update 5/8/21**

I went back and refactored the whole thing, more or less, because I pretty much just hacked it together the first time, and felt inspired to produce *clean code*. Also the scraping might've broken. As I was attempting to diagnose the problem I took a moment to reflect upon why I had decided to scrape two sites that had free, public apis in the first place. The answer I came up with was: because I felt like it. And today I felt like using the apis. Which is obviously the sensible choice. Turns out I didn't even have to deal with any tokens. The Wikimedia api was vaguely byzantine, but whatever.

I also built a new rest api, where once there was graphql, for the Next app to consume. Graphql is awesome but there was some weirdness, as detailed below, with how Apollo was interacting with Next.

And, as mentioned, I went back and refactored pretty much the whole app to make it read better. I removed almost all the comments in a, probably vain, attempt to make the code so expressive and clean that comments would only detract from its immaculate nature. I do enjoy refactoring, trying to come up with the perfect function name and so forth. It helps to have some time between the initial development and refactoring so that you can be fully baffled and disturbed by your previous choices.

AND, almost forgot, I taught it how to speak. NBD. I used [DeepAI](https://deepai.org) to generate (strange) captions for the photos. Going to add *Artificial Intelligence Researcher* to my resume now.

## The Next App

Probably the thing about Next that most makes it Next and not just React is its desire to build static pages, and if not at least render them server side. Some of that [it will do automatically](https://nextjs.org/docs/advanced-features/automatic-static-optimization), but where you have control over how the build operates is with their [two built in data fetching functions](https://nextjs.org/docs/basic-features/data-fetching): <code>getStaticProps()</code>, which fetches your data at build time; and <code>getServerSideProps()</code>, which fetches on request then renders on the server. I used Apollo to fetch the data via graphql queries. And Apollo has a cache, so if you make a query that's cached apollo will return the data from the cache instead of putting all the time and effort into querying the server.

This is the point where I admit that I kind of messed up. With the way I set up the data fetching I don't think I'm actually using <code>getStaticProps()</code>, I'm just using the Apollo cache. I followed [these instructions](https://www.apollographql.com/blog/building-a-next-js-app-with-apollo-client-slash-graphql) for wiring Apollo up to Next. It makes a query inside <code>getStaticProps()</code> and instantiates the cache like so:

~~~
export async function getStaticProps() {

	const apolloClient = initializeApollo()
	await apolloClient.query({ query: GET_POSTS })

	return addApolloState(apolloClient, {
		props: {},
		revalidate: 1,
	})
}
~~~

The idea is that Next then makes the data available to your collocated component via props. But in this case as you can see I'm actually getting my data from the Apollo cache.

~~~
export default function Index({apolloCache}) {

	const sortedPosts = sortPosts(apolloCache.ROOT_QUERY.getPosts)
...
~~~

And this is borne out by the fact that when the server produces a new image it shows up on the home page without a new site build. The intended behavior of <code>getStaticProps()</code> is to fetch the data once at build time and store it in the site bundle. Though, the current unintended behavior is actually what I wanted. Rebuilding the site when the server produces a new image every hour isn't ideal. What I don't understand is if the data fetching is happening inside <code>getStaticProps()</code> why is it fetching on request?

Probably what I should have done was use <code>getServerSideProps()</code>, but adding the Apollo cache to the mix complicates the situation. And having a cache is handy! Right now I'm just fetching the whole database once and then cruising the site at will and the only thing it's hitting the server for is images. That scheme will I'm sure require paging at some point as the number of records grow. And I didn't even try to use next's data fetching stuff on the individual image page. 

Basically I failed at <code>getStaticProps()</code> and <code>getServerSideProps()</code>. And as the data fetching functions are so central to what Next does I kind of failed at Next. Though it still does all sorts of code splitting and building and automatic static generation—so it's probably not totally pointless that I used it here. But the site deserves a refactor to better take advantage of what Next offers. I will have to think it through. The trade offs between client side, server side, and static aren't necessarily that easy to delineate. And trying to mix and match them in an isomorphic scheme is pretty complicated, even if you have a nice framework like next to hold your hand, and you're building a tiny site like this.

There's a bunch of other stuff next provides out of the box too. Their routing is nice, if you want a component to have a route you just put it in the pages directory and next provides it with a route. For instance this component lives at <code>[/pages/about.js](https://gitlab.com/merged-randos/merged-randos-site/-/blob/master/pages/about.js)</code>. And then you can link to it with their Link component. If you want dynamic routes put the file name in brackets like <code>[slug].js</code>. There are also more full featured routing tools if you need them. Next is pretty good about starting out simple and giving you access to lower level functionality as you need it. Seems like you can to a lot with their [config file](https://nextjs.org/docs/api-reference/next.config.js/introduction). They've also got optional [image](https://nextjs.org/docs/api-reference/next/image) and [CSS in JS](https://nextjs.org/docs/basic-features/built-in-css-support#css-in-js) tools, which aren't exactly the best in class but they're there and you don't have to use them if you don't want to. I did use them and they were fine.

**Update 5/8/21**

Attempting to get to the bottom of the weirdness with Apollo I ended up just ripping it out and replacing it with [SWR](https://swr.vercel.app) which is a data fetching/caching tool built by the same people who make Next. I figured why not use the thing built by the people who built the thing. It worked fine. It's pretty lightweight, it uses whatever fetching library you like (fetch in this case) and provides you with a handful of methods to deal with caching and whatnot. I believe I now have a Next app behaving as it is intended as far as static and server side rendering goes.

I also, as I was building a new api, took the opportunity to implement paging, prefetching, and prerendering. So, when getting data, the app functions like a real actual app now. And the disturbing white flash when loading images has been eliminated. 

## The iPhone App

I was thinking of adding something interactive to the site—something where people could merge their own randos—like they select the search terms and get a nice picture out of it? But it occurred to me that it would be much cooler to let them do it with their own photos. So I figured since I knew React why not try out React Native. I've never done any native development of any sort for anything anywhere. I was, until now, purely a web developer.  I used [Create React Native App](https://github.com/expo/create-react-native-app) to scaffold it. I have no idea what it does. But it worked. It uses things called [Expo](https://expo.io) and [Cocoapods](https://github.com/CocoaPods/CocoaPods) which I also do not understand.

My impression of React Native is that it's maybe best for making apps that are websites-like. That is apps that are doing a lot of their work on the server side and the app is more or less just a client. Which my app is not! Ha. I say this because in looking for image editing tools in react native I didn't find much. Maybe I needed to dig into the ios api, there's certainly a lot there. Though relying on it too much seems like it kind of defeats the purpose of using a cross platform framework.

What I ended up doing was just using React Native's built in Image and ImageBackground components to overlay two images and set the top one to 50% opacity. Then if you tap save it takes a screenshot of the component and saves that. Which isn't ideal as the component is small, so you're left with a small image. With more robust tools it would've been possible to save a bigger image, which is more useful to people. It was cool though, I made my first iPhone app! 

## App Privacy Policy

This is here because the app store requires it to be somewhere: the iPhone app collects no data, does not connect to the internet, everything just lives on your phone and belongs to you. So it is by design totally private. No one but you has any access to what goes on in the app.

## Contact
Email joe@joeschoech.com if you like. Send me some of the double exposures you made.
`;
