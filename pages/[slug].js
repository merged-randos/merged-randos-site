import { Fragment, useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import Image from 'next/image';
import useSWR from 'swr';
import { fetcher, useEvent, convertTimestamp } from '../utilities/helpers';
import NavButton from '../components/nav-button';

export default function Slug() {
	const [post, setPost] = useState(null);
	const router = useRouter();
	const { slug } = router.query;
	const { data, error } = useSWR(`${process.env.NEXT_PUBLIC_URI}/posts?slug=${slug}`, fetcher, {
		revalidateOnFocus: false,
	});

	if (data?.post && !post) {
		setPost(data.post);
	}

	const handleNav = (direction) => {
		if (data && data[direction]) {
			setPost(data[direction]);
			router.push(`/${data[direction].slug}`);
		}
	};

	const handleKeyDown = (event) => {
		if (event.keyCode == '37') {
			handleNav('previous');
		} else if (event.keyCode == '39') {
			handleNav('next');
		}
	};
	useEvent('keydown', handleKeyDown);

	if (error) {
		return <Fragment>Error</Fragment>;
	}
	if (!post && !error) {
		return <Fragment>Loading</Fragment>;
	}
	return (
		<Fragment>
			<Head>
				<title>{post.title}</title>
			</Head>
			<main className="flex-container">
				<section className="photo-section">
					<Image src={`${process.env.NEXT_PUBLIC_URI}/images/${post.image}`} alt={post.title} priority width={600} height={600} />
				</section>
				<section className="text-section">
					<h1>{post.title}</h1>
					<div>{post.caption}</div>
					<h3>
						Sources:{` `}
						<a href={post.sources[0].page} target="_blank">
							{post.sources[0].term}
						</a>
						,{' '}
						<a href={post.sources[1].page} target="_blank">
							{post.sources[1].term}
						</a>
					</h3>
					<h4>{convertTimestamp(post.timestamp)}</h4>
					<h2>
						<NavButton data={data} direction="previous" handleNav={handleNav} />
						<NavButton data={data} direction="next" handleNav={handleNav} />
					</h2>
				</section>
			</main>
			<div className="preload">
				{data?.next?.image && (
					<Image src={`${process.env.NEXT_PUBLIC_URI}/images/${data.next.image}`} alt={data.next.title} width={600} height={600} />
				)}
				{data?.previous?.image && (
					<Image src={`${process.env.NEXT_PUBLIC_URI}/images/${data.previous.image}`} alt={data.previous.title} width={600} height={600} />
				)}
			</div>
			<style jsx>{`
				.flex-container {
					display: flex;
					justify-content: space-between;
					flex-direction: row-reverse;
				}
				.photo-section {
					width: 63%;
				}
				.text-section {
					width: 37%;
					padding-right: 20px;
				}
				.preload {
					width: 1px;
					height: 1px;
					opacity: 1%;
				}
				.spacer {
					margin: 0 5px;
				}
				h1 {
					margin: 0;
				}
				h2,
				h3,
				h4,
				h5 {
					margin: 0.5em 0;
				}
				@media (max-width: 960px) {
					.flex-container {
						display: block;
					}
					.photo-section {
						width: 100%;
					}
					.text-section {
						width: 100%;
						padding-right: 0;
					}
				}
			`}</style>
		</Fragment>
	);
}
