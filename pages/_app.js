import '../styles/globals.css';
import Header from '../components/header';
import { Fragment } from 'react';

export default function App({ Component, pageProps }) {
	return (
		<Fragment>
			<Header />
			<Component {...pageProps} />
		</Fragment>
	);
}
